package util;

import tenrec.model.Genre;
import tenrec.model.Movie;
import tenrec.model.MovieBuilder;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestModelFactory {
    private static String[] G_NAMES = {"Akční", "Dobrodružný", "Horor", "Animovaný",
            "Drama", "Fantasy", "Sci-Fi", "Sportovní", "Mysteriozní"};

    public static List<Movie> getFilterTestModel(){
        List<Movie> model = new ArrayList<>();
        List<List<Genre>> genres = new ArrayList<>();

        for (int i = 0; i < 4; ++i) {
            genres.add(new ArrayList<>());
        }

        Genre g0 = new Genre(G_NAMES[0], 0);
        Genre g1 = new Genre(G_NAMES[1], 1);
        Genre g2 = new Genre(G_NAMES[2], 2);
        Genre g3 = new Genre(G_NAMES[4], 4);
        Genre g4 = new Genre(G_NAMES[5], 5);

        Collections.addAll(genres.get(0), g0, g1, g2);
        Collections.addAll(genres.get(1), g3, g4, g2);
        Collections.addAll(genres.get(2), g3);
        Collections.addAll(genres.get(3), g0, g4);

        model.add(new MovieBuilder().setName("movie with weird name").setUID(0)
            .setRelease(1998).setLength(123).setGenres(genres.get(0)).toMovie());

        model.add(new MovieBuilder().setName("funny drama horror").setUID(1)
            .setRelease(2003).setLength(63).setGenres(genres.get(1)).toMovie());

        model.add(new MovieBuilder().setName("Luban the spy").setUID(2)
            .setRelease(1993).setLength(204).setGenres(genres.get(2)).toMovie());

        model.add(new MovieBuilder().setName("Spytihněvův koutek").setUID(3)
            .setRelease(2013).setLength(243).setGenres(genres.get(3)).toMovie());

        model.add(new MovieBuilder().setName("Lachtaní Maminka").setUID(4)
            .setRelease(2003).setLength(73).setGenres(genres.get(2)).toMovie());

        model.add(new MovieBuilder().setName("Pár pařmenů").setUID(5)
            .setRelease(2006).setLength(148).setGenres(genres.get(0)).toMovie());
        return model;
    }


    public static List<Genre> getGenreListStringConverterTestModel(){
        Genre g0 = new Genre(G_NAMES[0], 0);
        Genre g1 = new Genre(G_NAMES[1], 1);
        Genre g2 = new Genre(G_NAMES[2], 2);
        Genre g3 = new Genre(G_NAMES[3], 3);
        Genre g4 = new Genre(G_NAMES[4], 4);

        List<Genre> model = new ArrayList<>();

        Collections.addAll(model, g0, g1, g2, g3, g4);

        return model;
    }

    public static List<Movie> getSpecialValueKeeperTestModel(){
        return getFilterTestModel();
    }
}
