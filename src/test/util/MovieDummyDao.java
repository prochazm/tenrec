package util;

import com.sun.javafx.robot.FXRobotFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tenrec.database.IMovieDao;
import tenrec.model.Movie;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MovieDummyDao implements IMovieDao {
    List<Movie> movies = FXCollections.observableArrayList();

    @Override
    public Movie readLongest() {
        return movies.stream().max(Comparator.comparingInt(Movie::getLength)).orElse(null);
    }

    @Override
    public Movie readShortest() {
        return movies.stream().min(Comparator.comparingInt(Movie::getLength)).orElse(null);
    }

    @Override
    public Movie readNewest() {
        return movies.stream().max(Comparator.comparingInt(Movie::getReleaseYear)).orElse(null);
    }

    @Override
    public Movie readOldest() {
        return movies.stream().min(Comparator.comparingInt(Movie::getReleaseYear)).orElse(null);
    }

    @Override
    public Movie readByUserId(int userId) {
        return movies.stream().filter(e -> e.getUserId() == userId).findFirst().orElse(null);
    }

    @Override
    public Integer getFirstEmptyUserId() {
        int i = 1;
        while (readByUserId(0) != null){
            ++i;
        }
        return i;
    }

    @Override
    public void create(Movie object) {
        movies.add(object);
    }

    @Override
    public Movie read(Integer id) {
        return movies.stream().filter(e -> e.getId() == id).findFirst().orElse(null);
    }

    @Override
    public void update(Movie object) {
        if (!movies.contains(object)) {
            create(object);
        }
    }

    @Override
    public void delete(Movie object) {
        movies.remove(object);
    }

    @Override
    public ObservableList<Movie> getAll() {
        return (ObservableList<Movie>) movies;
    }
}
