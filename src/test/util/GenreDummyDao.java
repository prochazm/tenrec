package util;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tenrec.database.IGenreDao;
import tenrec.model.Genre;

import java.util.List;

public class GenreDummyDao implements IGenreDao {
    List<Genre> genres = FXCollections.observableArrayList();

    @Override
    public Genre readByName(String name) {
        return genres.stream().filter(e -> e.getGenre().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    @Override
    public void create(Genre object) {
        genres.add(object);
    }

    @Override
    public Genre read(Integer id) {
        return genres.stream().filter(e -> e.getId() == id).findFirst().orElse(null);
    }

    @Override
    public void update(Genre object) {
        if (!genres.contains(object)){
            create(object);
        }
    }

    @Override
    public void delete(Genre object) {
        genres.remove(object);
    }

    @Override
    public ObservableList<Genre> getAll() {
        return (ObservableList<Genre>)genres;
    }
}
