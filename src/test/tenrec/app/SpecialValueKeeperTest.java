package tenrec.app;

import org.junit.Before;
import org.junit.Test;
import tenrec.database.IMovieDao;
import tenrec.model.Movie;
import util.MovieDummyDao;
import util.TestModelFactory;

import java.util.List;

import static org.junit.Assert.*;

public class SpecialValueKeeperTest {
    private SpecialValueKeeper specialValueKeeper;
    private List<Movie> model;
    private IMovieDao dao;

    @Before
    public void setup(){
        dao = new MovieDummyDao();
        specialValueKeeper = new SpecialValueKeeper(dao);
        model = TestModelFactory.getSpecialValueKeeperTestModel();
        model.forEach(m -> dao.create(m));
    }

    @Test
    public void getLongest_checkValue(){
        Movie m = dao.readLongest();
        int lengthByKeeper = specialValueKeeper.getLongestMovieLength().get();
        int length = (m == null) ? 0 : m.getLength();

        assertEquals("Longest movie length:", length, lengthByKeeper);
    }

    @Test
    public void getShortest_checkValue(){
        Movie m = dao.readShortest();
        int lengthByKeeper = specialValueKeeper.getShortestMovieLength().get();
        int length = (m == null) ? 0 : m.getLength();

        assertEquals("Shortest movie length:", length, lengthByKeeper);
    }

    @Test
    public void getNewest_checkValue(){
        Movie m = dao.readNewest();
        int releaseByKeeper = specialValueKeeper.getNewestMovieRelease().get();
        int release = (m == null) ? 0 : m.getReleaseYear();

        assertEquals("Newest movie release year:", release, releaseByKeeper);
    }

    @Test
    public void getOldest_checkValue(){
        Movie m = dao.readOldest();
        int releaseByKeeper = specialValueKeeper.getOldestMovieRelease().get();
        int release = (m == null) ? 0 : m.getReleaseYear();

        assertEquals("Oldest movie release year:", release, releaseByKeeper);
    }

    @Test
    public void getLongest_afterModelModification_checkValue(){
        dao.delete(model.get(3));
        getLongest_checkValue();
    }

    @Test
    public void getShortest_afterModelModification_checkValue(){
        dao.delete(model.get(1));
        getShortest_checkValue();
    }

    @Test
    public void getNewest_afterModelModification_checkValue(){
        dao.delete(model.get(3));
        getNewest_checkValue();
    }

    @Test
    public void getOldest_afterModelModification_checkValue(){
        dao.delete(model.get(2));
        getNewest_checkValue();
    }

    @Test
    public void getLongest_withEmptyModel_zeroExpected(){
        model.forEach(m -> dao.delete(m));
        getLongest_checkValue();
    }

    @Test
    public void getShortest_withEmptyModel_zeroExpected(){
        model.forEach(m -> dao.delete(m));
        getShortest_checkValue();
    }

    @Test
    public void getNewest_withEmptyModel_zeroExpected(){
        model.forEach(m -> dao.delete(m));
        getNewest_checkValue();
    }

    @Test
    public void getOldest_withEmptyModel_zeroExpected(){
        model.forEach(m -> dao.delete(m));
        getOldest_checkValue();
    }
}
