package tenrec.app;

import javafx.collections.FXCollections;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import tenrec.database.IDataSource;
import tenrec.model.Genre;
import tenrec.model.Movie;
import util.TestModelFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MovieFilterTest {
    private static List<Movie> model;
    private static IDataSource<Movie> source;

    private MovieFilter filter;

    @BeforeClass
    public static void setupClass(){
        model = TestModelFactory.getFilterTestModel();
        source = () -> FXCollections.observableArrayList(model);
    }

    @Before
    public void setup(){
        this.filter = new MovieFilter(source);
    }

    @Test
    public void filterRelease_olderThan_multipleRecords(){
        this.filter.setOlderThanFilter(2000);

        assertEquals("Result size: ", 2, filter.getFilteredList().size());
    }

    @Test
    public void filterRelease_OlderThanEdgeCase_multipleRecords(){
        this.filter.setOlderThanFilter(1998);

        assertEquals("Result size:", 2, filter.getFilteredList().size());
    }

    @Test
    public void filterRelease_newerThan_multipleRecords(){
        this.filter.setNewerThanFilter(2000);

        assertEquals("Result size:", 4, filter.getFilteredList().size());
    }


    @Test
    public void filterRelease_newerThanEdgeCase_multipleRecords(){
        this.filter.setNewerThanFilter(2003);

        assertEquals("Result size:", 4, filter.getFilteredList().size());
    }

    @Test
    public void filterRelease_inRange_multipleRecords(){
        this.filter.setOlderThanFilter(2004);
        this.filter.setNewerThanFilter(1995);

        assertEquals("Result size:", 3, filter.getFilteredList().size());
    }

    @Test
    public void filterRelease_inRangeEdgeCase_multipleRecords(){
        this.filter.setOlderThanFilter(2003);
        this.filter.setNewerThanFilter(1998);

        assertEquals("Result size:", 3, filter.getFilteredList().size());
    }

    @Test
    public void filterLength_longerThan_multipleRecords(){
        this.filter.setLongerThanFilter(200);

        assertEquals("Result size:", 2, filter.getFilteredList().size());
    }

    @Test
    public void filterLength_longerThanEdgeCase_multipleRecords(){
        this.filter.setLongerThanFilter(204);

        assertEquals("Result size:", 2, filter.getFilteredList().size());
    }

    @Test
    public void filterLength_shorterThan_multipleRecords(){
        this.filter.setShorterThanFilter(130);

        assertEquals("Result size:", 3, filter.getFilteredList().size());
    }

    @Test
    public void filterLength_shorterThanEdgeCase_multipleRecords(){
        this.filter.setShorterThanFilter(123);

        assertEquals("Result size:", 3, filter.getFilteredList().size());
    }

    @Test
    public void filterLength_inRange_multipleRecords(){
        this.filter.setLongerThanFilter(130);
        this.filter.setShorterThanFilter(210);

        assertEquals("Result size:", 2, filter.getFilteredList().size());
    }

    @Test
    public void filterLength_inRangeEdgeCase_multipleRecords(){
        this.filter.setLongerThanFilter(123);
        this.filter.setShorterThanFilter(204);

        assertEquals("Result size:", 3, filter.getFilteredList().size());
    }

    @Test
    public void filterGenre_belongsToOne_multipleRecords(){
        List<Genre> genreFilterList = new ArrayList<>();
        genreFilterList.add(model.get(0).getGenres().get(2));

        this.filter.setGenreFilter(genreFilterList);

        assertEquals("Result size:", 3, filter.getFilteredList().size());
    }

    @Test
    public void filterGenre_BelongsToAny_multipleRecords(){
        List<Genre> genreFilterList = new ArrayList<>();
        genreFilterList.add(model.get(1).getGenres().get(0));
        genreFilterList.add(model.get(1).getGenres().get(2));

        this.filter.setGenreFilter(genreFilterList);

        assertEquals("Result size:", 5, filter.getFilteredList().size());
    }

    @Test
    public void filterName_contains_multipleRecords(){
        this.filter.setNameFilter("th");

        assertEquals("Result size:", 2, filter.getFilteredList().size());
    }

    @Test
    public void filterName_containsIgnoreCase_multipleRecords(){
        this.filter.setNameFilter("spy");

        assertEquals("Result size:", 2, filter.getFilteredList().size());
    }

    //todo predicate chaining tests
}