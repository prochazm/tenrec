package tenrec.app;

import org.junit.Before;
import org.junit.Test;
import tenrec.database.IGenreDao;
import tenrec.model.Genre;
import util.GenreDummyDao;
import util.TestModelFactory;

import java.util.List;

import static org.junit.Assert.*;

public class GenreListStringConverterTest {

    private IGenreDao dao;
    private GenreListStringConverter converter;

    @Before
    public void setup(){
        dao = new GenreDummyDao();
        converter = new GenreListStringConverter(dao);
        List<Genre> model = new TestModelFactory().getGenreListStringConverterTestModel();
        for (Genre genre : model) {
            dao.create(genre);
        }
    }

    @Test
    public void fromString_containedValues_multipleRecords(){
        List<Genre> genres = converter.fromString("Akční, Dobrodružný, Horor");

        assertEquals("Converted genres count:", 3, genres.size());
        assertEquals("Model genres count:", 5, dao.getAll().size());
    }

    @Test
    public void fromString_uncontainedValues_multipleRecords(){
        List<Genre> genres = converter.fromString("Sci-Fi, Sportovní, Mysteriozní");

        assertEquals("Converted genres count:", 3, genres.size());
        assertEquals("Model genres count:", 8, dao.getAll().size());
    }

    @Test
    public void fromString_mixedValues_multipleRecords(){
        List<Genre> genres = converter.fromString("Akční, Sportovní, Horor");

        assertEquals("Converted genres count:", 3, genres.size());
        assertEquals("Model genres count:", 6, dao.getAll().size());
    }
}