package tenrec.database;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Connection
 *
 * Singleton class takes care of database
 * connectivity.
 */
public class Connection {
    private static final EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    private static final Connection instance;

    static {
        instance = new Connection();
        entityManagerFactory = Persistence.createEntityManagerFactory("movies");
    }

    private Connection(){}

    /**
     * Starts transaction and returns EntityManager
     * @return EntityManager instance
     */
    public EntityManager startTransaction(){
        if (this.entityManager != null && this.entityManager.isOpen()){
            this.endTransaction();
            System.out.println("closing previous transaction");
        }
        this.entityManager = entityManagerFactory.createEntityManager();
        this.entityManager.getTransaction().begin();
        return this.entityManager;
    }

    /**
     * Ends transaction
     */
    public void endTransaction(){
        if (this.entityManager == null || !this.entityManager.isOpen()){
            return;
        }
        if (this.entityManager.getTransaction().isActive()) {
            this.entityManager.flush();
            this.entityManager.getTransaction().commit();
        }
        this.entityManager.close();
    }

    /**
     * Closes EntityManagerFactory
     */
    public void close(){
        entityManagerFactory.close();
    }

    /**
     * Returns the singleton instance
     * @return instance of this class
     */
    public static Connection getInstance() {
        return instance;
    }
}
