package tenrec.database;

import tenrec.model.Movie;

/**
 * IMovieDao
 *
 * More specific extension to IDao interface for
 * managing entities of {@link Movie} type
 */
public interface IMovieDao extends IDao<Movie> {
    /**
     * Returns {@link Movie} with highest length property.
     * @return longest {@link Movie}
     */
    Movie readLongest();

    /**
     * Returns {@link Movie} with lowest length property.
     * @return shortest {@link Movie}
     */
    Movie readShortest();

    /**
     * Returns {@link Movie} with highest releaseYear
     * property.
     * @return longest {@link Movie}
     */
    Movie readNewest();

    /**
     * Returns {@link Movie} with lowest releaseYear
     * property.
     * @return longest {@link Movie}
     */
    Movie readOldest();

    /**
     * Returns {@link Movie} with userId equal to
     * int values passed as argument.
     * @param userId desired userId
     * @return {@link Movie} with such userId
     */
    Movie readByUserId(int userId);

    /**
     * Returns lowest positive Integer value
     * unused as userId.
     * @return first available userId
     */
    Integer getFirstEmptyUserId();
}
