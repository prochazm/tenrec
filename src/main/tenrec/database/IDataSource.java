package tenrec.database;

import javafx.collections.ObservableList;

import java.util.List;

/**
 * IDataSource
 *
 * Purpose of this interface is to restrict
 * access to the DAO implementing it to single
 * purpose which is fetching entities from
 * database.
 * @param <T> entity type
 */
public interface IDataSource<T> {
    /**
     * Returns ObservableList of all entities
     * @return ObservableList of entities
     */
    ObservableList<T> getAll();
}
