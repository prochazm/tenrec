package tenrec.database;

import tenrec.model.Genre;

/**
 * IGenreDao
 *
 * This is more specific extension to IDao
 * interface intended for managing {@link Genre}
 * entities.
 */
public interface IGenreDao extends IDao<Genre> {
    /**
     * Returns {@link Genre} with name equaling to
     * string passed as argument.
     * @param name desired name
     * @return {@link Genre} with desired name
     */
    Genre readByName(String name);
}
