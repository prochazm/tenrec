package tenrec.database;

import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tenrec.model.Movie;

import javax.persistence.EntityManager;
import java.util.Comparator;

/**
 * MovieDao
 *
 * This is implementation of IMovieDao and IDataSource.
 * It basically takes care of persisting Movies.
 * Read methods return null if nothing is found.
 */
public class MovieDao implements IMovieDao, IDataSource<Movie> {
    private Connection connection;
    private ObservableList<Movie> movies;


    /**
     * Class constructor takes reference to Connection
     * instance and fetches Movies from database.
     * @param connection connection
     */
    public MovieDao(Connection connection){
        this.connection = connection;
        this.fetch();
    }

    /**
     * Load all Movies from database.
     */
    private void fetch(){
        EntityManager em = this.connection.startTransaction();
        this.movies = FXCollections.observableArrayList(p -> new Observable[]{
                p.releaseYearProperty(),
                p.lengthProperty(),
                p.genresProperty(),
                p.userIdProperty()
        });
        this.movies.addAll(em.createQuery("select m from Movie m", Movie.class).getResultList());
        this.connection.endTransaction();
    }

    /**
     * Persists passed {@link Movie}.
     * @param movie Movie to be persisted
     */
    @Override
    public void create(Movie movie) {
        this.movies.add(movie);
        EntityManager em = this.connection.startTransaction();
        em.persist(movie);
        this.connection.endTransaction();
    }

    /**
     * Reads {@link Movie} by passed id (not userId)
     * @param id id
     * @return {@link Movie} with such id
     */
    @Override
    public Movie read(Integer id) {
        return movies.stream().filter(c -> c.getId() == id).findFirst().orElse(null);
    }

    /**
     * Updates {@link Movie} passed as argument.
     * @param movie {@link Movie} to be updated
     */
    @Override
    public void update(Movie movie) {
        EntityManager em = this.connection.startTransaction();
        em.merge(movie);
        this.connection.endTransaction();
    }

    /**
     * Removes {@link Movie} passed as argument.
     * @param movie {@link Movie} for removal
     */
    @Override
    public void delete(Movie movie) {
        if (movie == null){
            return;
        }
        this.movies.remove(movie);
        EntityManager em = this.connection.startTransaction();
        em.remove(em.contains(movie) ? movie : em.merge(movie));
        this.connection.endTransaction();
    }

    /**
     * Returns ObservableList of Movies which
     * contains all Movies stored in database
     * this List gets updated with each CRUD
     * operation this DAO offers.
     * @return ObservableList of Movies
     */
    @Override
    public ObservableList<Movie> getAll() {
        return this.movies;
    }

    /**
     * Returns {@link Movie} with highest length property.
     * @return longest {@link Movie}
     */
    @Override
    public Movie readLongest() {
        return this.movies.stream().max(Comparator.comparingInt(Movie::getLength)).orElse(null);
    }

    /**
     * Returns {@link Movie} with lowest length property.
     * @return shortest {@link Movie}
     */
    @Override
    public Movie readShortest() {
        return this.movies.stream().min(Comparator.comparingInt(Movie::getLength)).orElse(null);
    }

    /**
     * Returns {@link Movie} with highest releaseYear property.
     * @return newest {@link Movie}
     */
    @Override
    public Movie readNewest() {
        return this.movies.stream().max(Comparator.comparingInt(Movie::getReleaseYear)).orElse(null);
    }

    /**
     * Returns {@link Movie} with lowest releaseYear property.
     * @return oldest {@link Movie}
     */
    @Override
    public Movie readOldest() {
        return this.movies.stream().min(Comparator.comparingInt(Movie::getReleaseYear)).orElse(null);
    }

    /**
     * Returns {@link Movie} userId identical to int value
     * passed as argument.
     * @param userId desired userId
     */
    @Override
    public Movie readByUserId(int userId) {
        return this.movies.stream().filter(m -> m.getUserId() == userId).findFirst().orElse(null);
    }

    /**
     * Returns lowest positive Integer value
     * unused as userId.
     * @return first available userId
     */
    @Override
    public Integer getFirstEmptyUserId() {
        this.movies.sort(Comparator.comparingInt(Movie::getUserId));
        int emptyId = 0;
        while (emptyId++ < this.movies.size() && this.movies.get(emptyId - 1).getUserId() == emptyId);
        return emptyId;
    }
}
