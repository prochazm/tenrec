package tenrec.database;

import javafx.collections.ObservableList;

/**
 * IDao
 *
 * This interface describes generic DAO
 * @param <T> type of persisted entity
 */
public interface IDao<T> {
    /**
     * Persists passed entity
     * @param object entity
     */
    void create(T object);

    /**
     * Reads entity by its id
     * @param id id
     * @return entity with such id
     */
    T read(Integer id);

    /**
     * Updates passed entity.
     * @param object entity
     */
    void update(T object);

    /**
     * Removes entity.
     * @param object entity
     */
    void delete(T object);

    /**
     * Returns ObservableList of all entities.
     * @return ObservableList of entities
     */
    ObservableList<T> getAll();
}
