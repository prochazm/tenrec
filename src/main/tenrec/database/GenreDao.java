package tenrec.database;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tenrec.model.Genre;

import javax.persistence.EntityManager;

/**
 * GenreDao
 *
 * This is implementation of IGenreDao it basically
 * takes care of persisting Genres.
 * Read methods return null if nothing is found.
 */
public class GenreDao implements IGenreDao {
    private Connection connection;
    private ObservableList<Genre> genres;


    /**
     * Class constructor takes reference to Connection
     * instance and fetches Genres from database.
     * @param connection connection
     */
    public GenreDao(Connection connection){
        this.connection = connection;
        this.fetch();
    }

    /**
     * Load all Genres from database.
     */
    private void fetch(){
        EntityManager em = this.connection.startTransaction();
        this.genres = FXCollections.observableArrayList(
                em.createQuery("select g from Genre g", Genre.class).getResultList());
        this.connection.endTransaction();
    }

    /**
     * Reads Genre by its name and returns is.
     * This operation is case sensitive.
     * @param name name of genre
     * @return Genre with such name
     */
    @Override
    public Genre readByName(String name) {
        return this.genres.stream().filter(g -> g.getGenre().equals(name)).findFirst().orElse(null);
    }

    /**
     * Persists passed {@link Genre}.
     * @param genre {@link Genre} to be persisted
     */
    @Override
    public void create(Genre genre) {
        EntityManager em = this.connection.startTransaction();
        em.persist(genre);
        connection.endTransaction();
        genres.add(genre);
    }

    /**
     * Reads {@link Genre} by passed id
     * @param id id
     * @return {@link Genre} with such id
     */
    @Override
    public Genre read(Integer id) {
        return this.genres.stream().filter(g -> g.getId() == id).findFirst().orElse(null);
    }

    /**
     * Updates {@link Genre} passed as argument.
     * @param genre {@link Genre} to be updated
     */
    @Override
    public void update(Genre genre) {
        EntityManager em = this.connection.startTransaction();
        em.merge(genre);
        this.connection.endTransaction();
    }

    /**
     * Removes {@link Genre} passed as argument.
     * @param genre {@link Genre} for removal.
     */
    @Override
    public void delete(Genre genre) {
        EntityManager em = this.connection.startTransaction();
        em.remove(em.contains(genre) ? genre : em.merge(genre));
        this.connection.endTransaction();
        this.genres.remove(genre);
    }

    /**
     * Returns ObservableList of Genres which
     * contains all Genres stored in database
     * this List gets updated with each CRUD
     * operation this DAO offers.
     * @return ObservableList of Genres
     */
    @Override
    public ObservableList<Genre> getAll() {
        return genres;
    }
}
