package tenrec.model;

import java.util.List;

/**
 * MovieBuilder
 *
 * This is utility builder class for easy
 * {@link Movie} building.
 */
public class MovieBuilder {

    private Movie movie;

    public MovieBuilder(){
        this(new Movie());
    }

    public MovieBuilder(Movie movie){
        this.movie = movie;
    }

    public MovieBuilder setUID(int userId){
        this.movie.setUserId(userId);
        return this;
    }

    public MovieBuilder setName(String name){
        this.movie.setName(name);
        return this;
    }

    public MovieBuilder setRelease(int releaseYear){
        this.movie.setReleaseYear(releaseYear);
        return this;
    }

    public MovieBuilder setGenres(List<Genre> genres){
        this.movie.setGenres(genres);
        return this;
    }

    public MovieBuilder setLength(int length){
        this.movie.setLength(length);
        return this;
    }

    public MovieBuilder setSummary(String summary){
        this.movie.setSummary(summary);
        return this;
    }

    public Movie toMovie(){
        return movie;
    }
}
