package tenrec.model;

import javafx.beans.property.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "genre")
public class Genre implements Serializable{
    private StringProperty genre;
    private IntegerProperty id;

    public Genre(){
        this.genre = new SimpleStringProperty();
        this.id = new SimpleIntegerProperty();
    }

    public Genre(String name, int id){
        this();
        this.setGenre(name);
        this.setId(id);
    }

    @Column(name = "name")
    public String getGenre() {
        return genre.get();
    }

    public StringProperty genreProperty() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre.set(genre);
    }

    @Id
    @Column(name = "id")
    @GeneratedValue
    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    @Override
    public String toString(){
        return this.getGenre();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Genre)) return false;
        Genre genre = (Genre) o;
        return Objects.equals(getId(), genre.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
