package tenrec.model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;

import javax.persistence.*;
import java.io.*;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "movies")
@Access(AccessType.PROPERTY)
@TableGenerator(name="tab", initialValue=0, allocationSize=50)
public class Movie implements Serializable{

    private IntegerProperty id;
    private IntegerProperty userId;
    private StringProperty name;
    private IntegerProperty releaseYear;
    private ListProperty<Genre> genres;
    private IntegerProperty length;
    private StringProperty summary;

    public Movie(){
        id = new SimpleIntegerProperty();
        userId = new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        releaseYear = new SimpleIntegerProperty();
        genres = new SimpleListProperty<>();
        length = new SimpleIntegerProperty();
        summary = new SimpleStringProperty();
    }

    @Column(name = "summary", length = 2048)
    public String getSummary(){
        return this.summary.get();
    }

    public StringProperty summaryProperty() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary.set(summary);
    }

    @Column(name = "userId")
    public int getUserId(){
        return this.userId.get();
    }

    public IntegerProperty userIdProperty(){
        return this.userId;
    }

    public void setUserId(int userId){
        this.userId.set(userId);
    }

    @Column(name = "length")
    public int getLength() {
        return length.get();
    }

    public IntegerProperty lengthProperty() {
        return length;
    }

    public void setLength(int length) {
        this.length.set(length);
    }

    @ManyToMany
    @JoinTable(
            name="movie_genre",
            joinColumns = @JoinColumn(name="id_movie", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_genre", referencedColumnName = "id")
    )
    @Column(name = "genre")
    public List<Genre> getGenres() {
        return genres.get();
    }

    public void setGenres(List<Genre> genres) {
        if (genres == null){
            return;
        }
        this.genres.set(FXCollections.observableArrayList(genres));
    }

    public ListProperty<Genre> genresProperty() {
        return genres;
    }


    @Column(name = "release")
    public int getReleaseYear() {
        return releaseYear.get();
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear.set(releaseYear);
    }

    public IntegerProperty releaseYearProperty() {
        return releaseYear;
    }

    @Column(name = "name")
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty(){
        return name;
    }

    @GeneratedValue(strategy=GenerationType.TABLE, generator="tab")
    @Id
    @Column(name = "id")
    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.getId())
                .append(": ")
                .append(this.getName())
                .append(", length: ")
                .append(this.getLength())
                .append(", released: ")
                .append(this.getReleaseYear());
        if (!genres.isEmpty()) {
            sb.append(", genres: ")
                    .append(Arrays.toString(this.getGenres().toArray()));
        }

        return sb.toString();
    }
}
