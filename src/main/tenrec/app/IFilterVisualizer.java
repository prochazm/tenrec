package tenrec.app;

/**
 * IFilterVisualizer
 *
 * Marks class as filter feeder.
 */
public interface IFilterVisualizer {
    /**
     * Forces IFilterVisualizer to reset all filters
     */
    void resetFilter();
}
