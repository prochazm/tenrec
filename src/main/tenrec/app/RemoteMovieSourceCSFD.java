package tenrec.app;

import javafx.util.StringConverter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import tenrec.model.Genre;
import tenrec.model.Movie;

import java.io.IOException;
import java.util.List;

/**
 * {@code RemoteMovieSourceCSFD}
 *
 * This is implementation of IRemoteMovieSource which
 * uses csfd.cz as it's source.
 */
public class RemoteMovieSourceCSFD implements IRemoteMovieSource{
    private Movie movie;
    private Document page;
    private StringConverter<List<Genre>> genreListStringConverter;

    /**
     * Class constructor specifying {@code csfd.cz} url of scanned movie
     * and GenreListStringConverter instance to be used for
     * parsing genres.
     * @param url movie url
     * @param genreListStringConverter genre converter
     * @throws IOException in case there are troubles connecting to the url
     */
    RemoteMovieSourceCSFD(String url, GenreListStringConverter genreListStringConverter) throws IOException {
        this.genreListStringConverter = genreListStringConverter;
        this.page = Jsoup.connect(url).get();
    }

    /**
     * Call this method to parse set url into Movie instance.
     * @return movie instance
     */
    private Movie parseMovie() {
        String name, length, release, genresString, content, country;
        try {
            name = page
                    .getElementsByClass("header").first()
                    .getElementsByTag("h1").first()
                    .ownText();
            String[] countryAndLength = page
                    .getElementsByClass("origin").first()
                    .ownText().split(", , ");
            country = countryAndLength[0];
            length = countryAndLength[1].split(" ")[0];
            release = page
                    .getElementsByClass("origin").first()
                    .getElementsByAttributeValue("itemprop", "dateCreated").first()
                    .ownText();
            genresString = page
                    .getElementsByClass("genre").first()
                    .ownText().replace(" / ", ", ");
            content = page
                    .getElementById("plots")
                    .getElementsByClass("content").first()
                    .getElementsByAttribute("data-truncate").first()
                    .ownText();
        } catch (NullPointerException npe){
            throw new IllegalArgumentException("Page could not be parsed");
        }
        this.movie = new Movie();
        this.movie.setName(name);
        this.movie.setLength(Integer.parseInt(length));
        this.movie.setReleaseYear(Integer.parseInt(release));
        this.movie.setSummary(content);
        this.movie.setGenres(this.genreListStringConverter.fromString(genresString));

        return this.movie;
    }

    /**
     * Returns movie scanned from the url. It gets scanned
     * at the first call after that only stored instance
     * is returned.
     * @return scanned movie
     */
    @Override
    public Movie getMovie() {
        if (this.movie == null){
            return parseMovie();
        }
        return this.movie;
    }
}
