package tenrec.app;

import javafx.scene.control.Alert;
import javafx.util.converter.IntegerStringConverter;

/**
 * AlertPositiveIntegerStringConverter
 *
 * This class is extension to IntegerStringConverter
 * and its purpose is to convert Strings to positive
 * integers and vice versa, if it fails to do so then
 * Alert is fired and null returned.
 */
public class AlertPositiveIntegerStringConverter extends IntegerStringConverter {
    /**
     * Just calls super class.
     * @param value value for conversion
     * @return hopefully String
     */
    @Override
    public String toString(Integer value) {
        return super.toString(value);
    }


    /**
     * Tries to convert String to positive integer.
     * Fires Alert dialog and returns null on failure.
     * @param value converted String value
     * @return converted String as Integer
     */
    @Override
    public Integer fromString(String value) {
        try {
            int parsedValue = Integer.parseInt(value);
            if (parsedValue >= 0){
                return super.fromString(value);
            }
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Chyba editace");
            alert.setHeaderText(null);
            alert.setContentText("Vložte prosím nezáporné číslo.");
            alert.showAndWait();
        } catch(NumberFormatException nfe){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Chyba editace");
            alert.setHeaderText(null);
            alert.setContentText("Vložte prosím číslo.");
            alert.showAndWait();
        }
        return null;
    }
}
