package tenrec.app;

import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Modality;
import javafx.stage.Stage;
import tenrec.database.IDataSource;
import tenrec.database.IGenreDao;
import tenrec.database.IMovieDao;
import tenrec.model.Genre;
import tenrec.model.Movie;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * MovieTableController
 *
 * This is controller class sitting above {@code movieTable.fxml} it's
 * purpose is to handle user interaction with the table containing
 * application model ({@link Movie}s, {@link Genre}s). It also implements
 * IModelVisualizer interface making it a layer above DAOs which actually
 * handles things like selections, filtering, DAO exposure through gui
 * forms etc.
 */
public class MovieTableController implements IModelVisualiser {

    @FXML private TableView<Movie> movieTable;
    @FXML private TableColumn<Movie, Integer> idMovieTableCol;
    @FXML private TableColumn<Movie, String> nameMovieTableCol;
    @FXML private TableColumn<Movie, Integer> yearMovieTableCol;
    @FXML private TableColumn<Movie, Integer> lengthMovieTableCol;
    @FXML private TableColumn<Movie, List<Genre>> genreMovieTableCol;

    private IMovieDao movieDao;
    private IGenreDao genreDao;
    private MovieFilter movieFilter;
    private SortedList<Movie> movies;
    private ISpecialValueKeeper specialValueKeeper;
    private ObservableList<Genre> genres;
    private IFilterVisualizer filterVisualizer;

    /**
     * Class constructor initialized with desired {@link IMovieDao} and
     * {@link IGenreDao} implementation
     * @param movieDao desired {@link IMovieDao} implementation
     * @param genreDao desired {@link IGenreDao} implementation
     */
    public MovieTableController(IMovieDao movieDao, IGenreDao genreDao){
        this.movieDao = movieDao;
        this.genreDao = genreDao;

        this.movieFilter = new MovieFilter((IDataSource<Movie>)movieDao);
        this.specialValueKeeper = new SpecialValueKeeper(this.movieDao);

        this.movies = new SortedList<>(this.movieFilter.getFilteredList());
        this.genres = this.genreDao.getAll();
    }

    /**
     * Controller initializer.
     * Prepares gui components.
     */
    @FXML
    public void initialize(){
        idMovieTableCol.setCellFactory(TextFieldTableCell.forTableColumn(new AlertPositiveIntegerStringConverter()));
        nameMovieTableCol.setCellFactory(TextFieldTableCell.forTableColumn());
        yearMovieTableCol.setCellFactory(TextFieldTableCell.forTableColumn(new AlertPositiveIntegerStringConverter()));
        lengthMovieTableCol.setCellFactory(TextFieldTableCell.forTableColumn(new AlertPositiveIntegerStringConverter()));
        genreMovieTableCol.setCellFactory(TextFieldTableCell.forTableColumn(new GenreListStringConverter(this.genreDao)));

        nameMovieTableCol.setOnEditCommit(e -> {
            e.getRowValue().setName(e.getNewValue().trim());
            this.filterVisualizer.resetFilter();
            this.movieDao.update(e.getRowValue());
        });
        lengthMovieTableCol.setOnEditCommit(e -> {
            e.getRowValue().setLength(e.getNewValue() == null ? e.getOldValue() : e.getNewValue());
            this.filterVisualizer.resetFilter();
            this.movieDao.update(e.getRowValue());
            this.movieTable.refresh();
        });
        yearMovieTableCol.setOnEditCommit(e -> {
            e.getRowValue().setReleaseYear(e.getNewValue() == null ? e.getOldValue() : e.getNewValue());
            this.filterVisualizer.resetFilter();
            this.movieDao.update(e.getRowValue());
            this.movieTable.refresh();
        });
        idMovieTableCol.setOnEditCommit(e -> {
            if (e.getNewValue() == null){
                movieTable.refresh();
                return;
            }
            if (this.movieDao.readByUserId(e.getNewValue()) != null && !e.getOldValue().equals(e.getNewValue())){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Chyba editace");
                alert.setHeaderText(null);
                alert.setContentText("Tento identifikátor je již použit, zvolte jiný.");
                alert.showAndWait();
                movieTable.refresh();
                return;
            }
            e.getRowValue().setUserId(e.getNewValue());
            this.movieDao.update(e.getRowValue());
        });
        genreMovieTableCol.setOnEditCommit(e -> {
            e.getRowValue().setGenres(e.getNewValue());
            this.filterVisualizer.resetFilter();
            this.movieDao.update(e.getRowValue());
        });

        this.movieTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        this.movieTable.setItems(this.movies);
        this.movies.comparatorProperty().bind(this.movieTable.comparatorProperty());
    }

    /**
     * This method creates new independent window which
     * can be used for editing passed {@link Movie} instance.
     * After the window is created it's further handled by
     * {@link EditController}
     * @param movie instance of {@link Movie} to be edited by user
     */
    private void showEditWindow(Movie movie){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/editWindow.fxml"));
            Parent editRoot = loader.load();
            EditController ec = loader.getController();
            ec.setEditedMovie(movie, this.movieDao, this.genreDao);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Editovat film");
            stage.setScene(new Scene(editRoot, 550, 400));
            stage.showAndWait();
            if (filterVisualizer != null){
                filterVisualizer.resetFilter();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes selected movie(s) from database
     */
    @Override
    public void removeSelected(){
        List<Movie> selection = new ArrayList<>(movieTable.getSelectionModel().getSelectedItems());
        selection.forEach(this.movieDao::delete);
        this.movieTable.getSelectionModel().clearSelection();
    }

    /**
     * Returns single selected selected movie
     * @return selected movie
     */
    @Override
    public Movie getSelectedMovie(){
        return movieTable.getSelectionModel().getSelectedItem();
    }

    /**
     * Returns all selected movies
     * @return selected movies
     */
    @Override
    public List<Movie> getSelectedMovies() {
        return movieTable.getSelectionModel().getSelectedItems();
    }

    /**
     * Returns all visible movies meaning filters apply
     * @return visible movies
     */
    @Override
    public List<Movie> getMovies(){
        return movies;
    }

    /**
     * Returns all genres (not just those visible)
     * @return all genres
     */
    @Override
    public List<Genre> getGenres() {
        return genres;
    }

    /**
     * Returns reference to {@link IMovieFilter} implementation
     * used by this Controller
     * @return {@link IMovieFilter} instance
     */
    @Override
    public IMovieFilter getMovieFilter(){
        return movieFilter;
    }

    /**
     * Returns reference to {@link ISpecialValueKeeper} implementation
     * used by this Controller
     * @return {@link ISpecialValueKeeper} instance
     */
    @Override
    public ISpecialValueKeeper getSpecialValueKeeper() {
        return specialValueKeeper;
    }

    /**
     * Allows user to edit selected movie using
     * some GUI window.
     */
    @Override
    public void guiEditSelected(){
        if (this.movieTable.getSelectionModel().getSelectedItems().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Chyba editace");
            alert.setHeaderText(null);
            alert.setContentText("Označte film, který chcete editovat.");
            alert.showAndWait();
            return;
        }
        showEditWindow(this.movieTable.getSelectionModel().getSelectedItem());
    }

    /**
     * Allows user to add movie using some
     * GUI window.
     */
    @Override
    public void guiCreate(){
        showEditWindow(new Movie());
    }

    /**
     * Sets reference to IFilterVisualiser which can be
     * further used to force reset of filters.
     * @param filterVisualizer instance of IFilterVisualizer
     */
    @Override
    public void setFilterVisualizer(IFilterVisualizer filterVisualizer) {
        this.filterVisualizer = filterVisualizer;
    }

}
