package tenrec.app;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import org.controlsfx.control.CheckComboBox;
import org.controlsfx.validation.*;
import tenrec.database.IGenreDao;
import tenrec.database.IMovieDao;
import tenrec.model.Genre;
import tenrec.model.Movie;

import java.io.IOException;
import java.util.Optional;

/**
 * EditController
 *
 * This class is controller for Edit/Add window
 */
public class EditController {
    @FXML private CheckComboBox<Genre> genreComboCheck = new CheckComboBox<>();
    @FXML private TextField idTextField = new TextField();
    @FXML private Button saveButton = new Button();
    @FXML private TextField nameTextField = new TextField();
    @FXML private TextField lengthTextField = new TextField();
    @FXML private TextField releaseYearTextField= new TextField();
    @FXML private TextArea summaryTextArea = new TextArea();

    private Movie movie;
    private IMovieDao movieDao;
    private IGenreDao genreDao;

    private ValidationSupport validationSupport = new ValidationSupport();

    /**
     * Initializes gui.
     */
    @FXML
    public void initialize() {
        validationSupport.registerValidator(this.idTextField, true, this::validateUserId);
        validationSupport.registerValidator(this.nameTextField, true, this::validateStringEmpty);
        validationSupport.registerValidator(this.releaseYearTextField, true, this::validatePositiveInteger);
        validationSupport.registerValidator(this.lengthTextField, true, this::validatePositiveInteger);
    }

    /**
     * Validator method for {@link Movie}'s userId property.
     * UserId is considered valid if it's positive
     * integer that is unique amongst all {@link Movie}s.
     * @param control GUI element containing validated data
     * @param content String to be validated
     * @return {@link ValidationResult}
     */
    private ValidationResult validateUserId(Control control, String content){
        ValidationResult integerValidationResult = this.validatePositiveInteger(control, content);
        if (integerValidationResult.getErrors().size() != 0){
            return integerValidationResult;
        }
        int id = Integer.parseInt(content);
        return ValidationResult.fromMessageIf(control,
                "tento identifikátor je obsazen", Severity.ERROR,
                this.movieDao.readByUserId(id) != null && id != this.movie.getUserId()
        );
    }

    /**
     * Validator method for positive integers values.
     * Meaning content is considered valid if it's
     * positive integer. {@link ValidationResult} instance
     * is returned.
     * @param control GUI element containing validated data
     * @param content String to be validated
     * @return {@link ValidationResult}
     */
    private ValidationResult validatePositiveInteger(Control control, String content){
        return ValidationResult.fromMessageIf(control,"musí být číslo", Severity.ERROR, !content.matches("\\d+"));
    }

    /**
     * Validator method for positive integers values.
     * Meaning content is considered valid if it's
     * positive integer. {@link ValidationResult} instance
     * is returned.
     * @param control GUI element containing validated data
     * @param content String to be validated
     * @return {@link ValidationResult}
     */
    private ValidationResult validateStringEmpty(Control control, String content){
        return ValidationResult.fromMessageIf(control,"řetězec je prázdný",
                Severity.ERROR,content == null || content.trim().equals("")
        );
    }

    /**
     * Setups window for editing passed Movie.
     * IMovieDao and IGenreDao implementation is needed
     * for persisting final changes made to data model.
     * @param movie edited Movie
     * @param movieDao IMovieDao implementation
     * @param genreDao IGenreDao implementation
     */
    public void setEditedMovie(Movie movie, IMovieDao movieDao, IGenreDao genreDao){
        this.movie = movie;
        this.movieDao = movieDao;
        this.genreDao = genreDao;
        this.genreComboCheck.getItems().addAll(genreDao.getAll());
        resetForm(this.movie, true);
    }

    /**
     * Persists changes made to edited movie
     * if filled data comply validation rules.
     * Then the window closes.
     * @param event event
     */
    @FXML
    public void saveAction(ActionEvent event) {
        if (validationSupport.isInvalid()){
            return;
        }
        this.movie.setName(this.nameTextField.getText());
        this.movie.setLength(Integer.parseInt(this.lengthTextField.getText()));
        this.movie.setReleaseYear(Integer.parseInt(this.releaseYearTextField.getText()));
        this.movie.setSummary(this.summaryTextArea.getText());
        this.movie.setUserId(Integer.parseInt(this.idTextField.getText()));
        this.movie.setGenres(this.genreComboCheck.getCheckModel().getCheckedItems());

        if (this.movie.getId() == 0) { // id should be zero if movie is not persisted yet
            this.movieDao.create(this.movie);
        } else {
            this.movieDao.update(this.movie);
        }
        ((Stage)((Control)event.getSource()).getScene().getWindow()).close();
    }

    /**
     * resetForm(,) wrapper callable from gui.
     * @param event event
     */
    @FXML
    public void resetAction(ActionEvent event) {
        this.resetForm(this.movie, true);
    }

    /**
     * Resets filled fields to values stored in edited Movie
     * instance. If second parameter is true then it also calls
     * resetFormId() method.
     * @param movie edited {@link Movie}
     * @param resetId if resetFormId() should be called
     */
    private void resetForm(Movie movie, boolean resetId){
        this.nameTextField.setText(movie.getName());
        this.lengthTextField.setText(Integer.toString(movie.getLength()));
        this.releaseYearTextField.setText(Integer.toString(movie.getReleaseYear()));
        this.summaryTextArea.setText(movie.getSummary());
        this.genreComboCheck.getItems().clear();
        this.genreComboCheck.getItems().addAll(genreDao.getAll());
        if (movie.getGenres() != null) {
            movie.getGenres().forEach(g -> this.genreComboCheck.getCheckModel().check(g));
        }
        if (resetId){
            this.resetFormId();
        }
    }

    /**
     * Resets filled userId. The rule is:
     * If Movies doesn't have it yet meaning it's zero
     * then first available one is used otherwise id
     * from the actual Movie instance is used instead.
     */
    private void resetFormId(){
        this.idTextField.setText(this.movie.getId() != 0 ?
                Integer.toString(this.movie.getUserId()) :
                Integer.toString(movieDao.getFirstEmptyUserId()));
    }

    /**
     * Just closes the window.
     * @param event event
     */
    @FXML
    public void cancelAction(ActionEvent event) {
        ((Stage)((Control)event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Pops up dialog which user can fill csfd.cz link in
     * to prefill the edit dialog with data from csfd.cz.
     * @param event event
     */
    public void loadFromAction(ActionEvent event) {
        TextInputDialog dialog = new TextInputDialog("url");
        dialog.setTitle("Načíst z csfd.cz");
        dialog.setHeaderText("Csfd stahovátko");
        dialog.setContentText("Vložte url filmu");
        Optional<String> url = dialog.showAndWait();
        if (url.isPresent()) {
            try {
                IRemoteMovieSource rms = new RemoteMovieSourceCSFD(url.get(),
                    new GenreListStringConverter(this.genreDao)); // reference to Richard Matthew Stallman? Not at all!
                this.resetForm(rms.getMovie(), false);
            } catch (IllegalArgumentException | IOException e) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Chyba načtení");
                alert.setHeaderText("Film nemohl být načten");
                alert.setContentText("Zkontrolujte zda je zadané url správné a stránka obsahuje všechny nutné údaje");
                alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                alert.showAndWait();
                this.loadFromAction(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
