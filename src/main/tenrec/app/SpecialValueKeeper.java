package tenrec.app;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ListChangeListener;
import tenrec.database.IMovieDao;
import tenrec.model.Movie;

/**
 * {@code SpecialValueKeeper}
 *
 * This is simple implementation of {@link ISpecialValueKeeper}. Which purpose
 * is to keep track of length of the longest/shortest and release year of
 * the newest/oldest {@link Movie}. All these values are stored as properties so
 * they can be easily observed.
 */
public class SpecialValueKeeper implements ISpecialValueKeeper {
    private IntegerProperty longestMovieLength = new SimpleIntegerProperty();
    private IntegerProperty shortestMovieLength = new SimpleIntegerProperty();
    private IntegerProperty oldestMovieRelease = new SimpleIntegerProperty();
    private IntegerProperty newestMovieRelease = new SimpleIntegerProperty();

    private IMovieDao dao;

    /**
     * Class constructor. {@link IMovieDao} instance is used as model which
     * this instance of SpecialValueKeeper will be listening to.
     * @param dao instance of {@link IMovieDao}
     */
    public SpecialValueKeeper(IMovieDao dao) {
        this.dao = dao;
        this.dao.getAll().addListener(new Listener());
        this.findMovies();
    }

    /**
     * Synchronize properties with DAO.
     */
    private void findMovies(){
        Movie longest = this.dao.readLongest();
        Movie shortest = this.dao.readShortest();
        Movie oldest = this.dao.readOldest();
        Movie newest = this.dao.readNewest();
        this.longestMovieLength.set(longest != null ? longest.getLength() : 0);
        this.shortestMovieLength.set(shortest != null && shortest != longest ? shortest.getLength() : 0);
        this.newestMovieRelease.set(newest != null ? newest.getReleaseYear() : 0);
        this.oldestMovieRelease.set(oldest != null && oldest != newest? oldest.getReleaseYear() : 0);
    }

    /**
     * Returns length of longest {@link Movie} stored as {@code IntegerProperty}
     * @return length of longest {@link Movie}
     */
    @Override
    public IntegerProperty getLongestMovieLength() {
        return this.longestMovieLength;
    }

    /**
     * Returns length of shortest {@link Movie} stored as {@code IntegerProperty}
     * @return length of shortest {@link Movie}
     */
    @Override
    public IntegerProperty getShortestMovieLength() {
        return this.shortestMovieLength;
    }

    /**
     * Returns release year of newest {@link Movie} as {@code IntegerProperty}
     * @return release year of newest {@link Movie}
     */
    @Override
    public IntegerProperty getNewestMovieRelease() {
        return this.newestMovieRelease;
    }

    /**
     * Return release year of oldest {@link Movie} as {@code IntegerProperty}
     * @return release year of oldest {@link Movie}
     */
    @Override
    public IntegerProperty getOldestMovieRelease() {
        return this.oldestMovieRelease;
    }

    /**
     * Listener
     *
     * Purpose of this listener is to keep properties of outer class
     * synchronized with DAO.
     */
    private class Listener implements ListChangeListener<Movie> {
        /**
         * Performs findMovies() call everytime change other than
         * permutation is spotted.
         * @param change what has been changed
         */
        @Override
        public void onChanged(Change<? extends Movie> change) {
            boolean permsOnly = true;
            while(change.next()){
                if(!change.wasPermutated()){
                    permsOnly = false;
                    break;
                }
            }

            if (permsOnly){
                return;
            }

            findMovies();
        }
    }
}
