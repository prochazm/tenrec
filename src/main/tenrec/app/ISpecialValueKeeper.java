package tenrec.app;

import javafx.beans.property.IntegerProperty;

/**
 * ISpecialValueKeeper
 *
 * This interface is supposed to provide
 * actually interesting numbers.
 */
public interface ISpecialValueKeeper {
    /**
     * Returns length of the longest movie as property.
     * @return length as property
     */
    IntegerProperty getLongestMovieLength();

    /**
     * Returns length of the shortest movie as property.
     * @return length as property
     */
    IntegerProperty getShortestMovieLength();

    /**
     * Returns release year of newest movie as property.
     * @return release year as property
     */
    IntegerProperty getNewestMovieRelease();

    /**
     * Returns release year of oldest movie as property.
     * @return release year as property
     */
    IntegerProperty getOldestMovieRelease();
}
