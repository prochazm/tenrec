package tenrec.app;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tenrec.database.Connection;
import tenrec.database.IGenreDao;
import tenrec.database.IMovieDao;
import tenrec.model.Genre;
import tenrec.model.Movie;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * {@code RandomDataBuilder}
 *
 * Sole purporse of this class is to fill database with garbage.
 */
public class RandomDataBuilder {
    private IMovieDao movieDao;
    private IGenreDao genreDao;

    private String[] genres = {"Akční", "Dobrodružný", "Horor", "Animovaný",
            "Drama", "Fantasy", "Sci-Fi", "Sportovní", "Mysteriozní"};

    /**
     * Class constructor, DAOs are used for database access.
     * @param movieDao DAO implementing {@link IMovieDao}
     * @param genreDao DAO implementing {@link IGenreDao}
     */
    public RandomDataBuilder(IMovieDao movieDao, IGenreDao genreDao){
        this.movieDao = movieDao;
        this.genreDao = genreDao;
    }

    /**
     * Fill database with silly rubbish
     * @param moviesCount
     */
    public void fill(int moviesCount){
        for (int i = 0; i < this.genres.length; ++i){
            Genre g = new Genre();
            g.setGenre(this.genres[i]);
            genreDao.create(g);
        }

        List<Genre> genres = genreDao.getAll();
        Random rnd = new Random();
        for (int i = 0; i < moviesCount; ++i){
            int genresCount = rnd.nextInt(3) + 1;
            Movie movie = new Movie();
            movie.setUserId(this.movieDao.getFirstEmptyUserId());
            movie.setName(getRandomString() + "_"+this.movieDao.getFirstEmptyUserId());
            movie.setLength(rnd.nextInt(150) + 40);
            movie.setReleaseYear(rnd.nextInt(100) + 1920);
            ObservableList<Genre> g = FXCollections.observableArrayList();
            for (int j = 0; j < genresCount; ++j){
                int d = rnd.nextInt(genres.size());
                while (g.contains(genres.get(d))){
                    d = rnd.nextInt(genres.size());
                }
                g.add(genres.get(d));
            }
            movie.setGenres(g);
            this.movieDao.create(movie);
        }
    }

    /**
     * Generates spooky {@code String} of variable
     * length (0 to 99 characters) and returns it.
     * @return randomly generated string
     */
    private String getRandomString(){
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < rnd.nextInt(100); ++i){
            sb.append((char)(rnd.nextInt(25) + 97));
        }
        return sb.toString();
    }
}
