package tenrec.app;

import javafx.collections.transformation.FilteredList;

/**
 * IFilter
 *
 * This is generic filter interface.
 * @param <T> type of filtered object
 */
public interface IFilter<T> {
    /**
     * Returns FilteredList of filtered objects
     * @return filtered list
     */
    FilteredList<T> getFilteredList();
}
