package tenrec.app;

import tenrec.model.Genre;
import tenrec.model.Movie;

import java.util.List;

/**
 * IModelVisualiser
 *
 * This interface is the bottom most part
 * of model processing hierarchy, which looks
 * like this:
 *
 *           v- FilterVisualizer <---.
 * DAO -> Filter -> ModelVisualiser -'
 *    '-> SpecialValueKeeper -^
 *
 * It's actual purpose is to control what
 * user sees and somehow control it.
 */
public interface IModelVisualiser {
    /**
     * Removes Movies selected by user
     */
    void removeSelected();

    /**
     * Allows user to edit his selection.
     */
    void guiEditSelected();

    /**
     * Allow user to create new Movie.
     */
    void guiCreate();

    /**
     * Sets reference to filter visualizer
     * @param filterVisualizer filter visualizer
     */
    void setFilterVisualizer(IFilterVisualizer filterVisualizer);

    /**
     * Return selected {@link Movie}
     * @return selected Movie
     */
    Movie getSelectedMovie();

    /**
     * Return selected {@link Movie}s
     * @return selected Movies
     */
    List<Movie> getSelectedMovies();

    /**
     * Return visible {@link Movie}s
     * @return list of {@link Movie}s
     */
    List<Movie> getMovies();

    /**
     * Returns all {@link Genre}s
     * @return list of {@link Genre}s
     */
    List<Genre> getGenres();

    /**
     * Returns reference to used {@link IMovieFilter}
     * implementation.
     * @return filter reference
     */
    IMovieFilter getMovieFilter();

    /**
     * Returns reference to used {@link ISpecialValueKeeper}
     * implementation.
     * @return special value keeper reference
     */
    ISpecialValueKeeper getSpecialValueKeeper();
}
