package tenrec.app;

import javafx.collections.transformation.FilteredList;
import tenrec.database.IDataSource;
import tenrec.database.IMovieDao;
import tenrec.model.Genre;
import tenrec.model.Movie;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

/**
 * MovieFilter
 *
 * This is implementation of write only interface {@link IMovieFilter}
 * which further extends generic {@link IFilter}. Purpose of this class
 * is to sit between the DAO and object requesting collection of movies
 * filtered by some criteria such as length and release year.
 */
public class MovieFilter implements IMovieFilter{
    private FilteredList<Movie> filteredMovies;
    private IDataSource<Movie> source;

    private Predicate<Movie> namePredicate;
    private Predicate<Movie> shorterThanPredicate;
    private Predicate<Movie> longerThanPredicate;
    private Predicate<Movie> olderThanPredicate;
    private Predicate<Movie> newerThanPredicate;
    private Predicate<Movie> genrePredicate;

    /**
     * Class constructor takes reference to {@link IDataSource} of
     * Movies which will most probably by DAO implementing this
     * interface.
     * @param source
     */
    public MovieFilter(IDataSource<Movie> source){
        this.source = source;
        this.fetchData();
        this.resetPredicates();
        this.updateFilteringPredicate();
    }

    /**
     * Resets predicates so resulting collection is full set
     * of Movies.
     */
    private void resetPredicates(){
        namePredicate = m -> true;
        shorterThanPredicate = m -> true;
        longerThanPredicate = m -> true;
        olderThanPredicate = m -> true;
        newerThanPredicate = m -> true;
        genrePredicate = m -> true;
    }

    /**
     * Sets name filter. Movie has to case insensitively contain
     * it to be considered into result set.
     * @param nameFilter name substring
     */
    @Override
    public void setNameFilter(String nameFilter){
        this.namePredicate = m -> m.getName().toLowerCase().contains(nameFilter.toLowerCase());
        this.updateFilteringPredicate();
    }

    /**
     * Sets time in minutes which all movies have to be shorter
     * than to be considered into result set.
     * @param shorterThanFilter upper boundary of length
     */
    @Override
    public void setShorterThanFilter(Integer shorterThanFilter){
        this.shorterThanPredicate = m -> m.getLength() <= shorterThanFilter;
        this.updateFilteringPredicate();
    }

    /**
     * Sets time in minutes which all movies have to be longer
     * than to be considered into result set.
     * @param longerThanFilter lower boundary of length
     */
    @Override
    public void setLongerThanFilter(Integer longerThanFilter){
        this.longerThanPredicate = m -> m.getLength() >= longerThanFilter;
        this.updateFilteringPredicate();
    }

    /**
     * Sets year which all movies have to be older than to be
     * considered into result set.
     * @param olderThanFilter upper release boundary
     */
    @Override
    public void setOlderThanFilter(Integer olderThanFilter){
        this.olderThanPredicate = m -> m.getReleaseYear() <= olderThanFilter;
        this.updateFilteringPredicate();
    }

    /**
     * Sets year which all movies have to be newer than to be
     * considered into result set.
     * @param newerThanFilter lower release boundary
     */
    @Override
    public void setNewerThanFilter(Integer newerThanFilter) {
        this.newerThanPredicate = m -> m.getReleaseYear() >= newerThanFilter;
        this.updateFilteringPredicate();
    }

    /**
     * Set List of genres where each movie has to belong
     * to at least one of them to be considered into result set.
     * @param genres collection of genres
     */
    @Override
    public void setGenreFilter(List<Genre> genres) {
            this.genrePredicate = genres.isEmpty() ?
                    m -> true :
                    m -> !Collections.disjoint(genres, m.getGenres());
        this.updateFilteringPredicate();
    }

    /**
     * Fetches data from data source.
     */
    private void fetchData(){
        this.filteredMovies = new FilteredList<>(this.source.getAll());
    }

    /**
     * Updates predicated for the filtered list of movies.
     * It's call by each set...Filter() method so there
     * should be no reason to try to call it manually.
     */
    private void updateFilteringPredicate(){
        filteredMovies.setPredicate(
                 this.shorterThanPredicate
            .and(this.longerThanPredicate)
            .and(this.olderThanPredicate)
            .and(this.newerThanPredicate)
            .and(this.namePredicate)
            .and(this.genrePredicate)
        );
    }

    /**
     * Returns reference to filtered list of movies.
     * @return filtered movies
     */
    @Override
    public FilteredList<Movie> getFilteredList() {
        return this.filteredMovies;
    }
}
