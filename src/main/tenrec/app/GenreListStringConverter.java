package tenrec.app;

import javafx.collections.FXCollections;
import javafx.util.StringConverter;
import tenrec.database.IGenreDao;
import tenrec.model.Genre;

import java.util.Arrays;
import java.util.List;

/**
 * GenreListStringConverter
 *
 * This class takes care of converting string containing
 * {@link Genre} names separated by commas to actual {@link Genre} objects
 * provided by passed {@link IGenreDao} implementation and vice versa.
 * In case there is need to convert string to {@link Genre} not known
 * to DAO then it is created.
 */
public class GenreListStringConverter extends StringConverter<List<Genre>> {
    private IGenreDao dao;

    /**
     * Class constructor takes reference to IGenreDao implementation
     * @param dao genre DAO reference
     */
    public GenreListStringConverter(IGenreDao dao){
        this.dao = dao;
    }

    /**
     * Converts List of {@link Genre}s to String
     * @param genres List of {@link Genre}s
     * @return Genres as String
     */
    @Override
    public String toString(List<Genre> genres) {
        String genresAsString = Arrays.toString(genres.toArray());
        return  genresAsString.substring(1, genresAsString.length() - 1);
    }

    /**
     * Converts {@link Genre} names contained in string separated by commas
     * to actual Genre instances. If {@link Genre} with such name is not
     * known to DAO then it's created.
     * @param s String with {@link Genre} names
     * @return List of {@link Genre}s
     */
    @Override
    public List<Genre> fromString(String s) {
        String[] GenresAsStrings = s.split(", ");
        List<Genre> genres = FXCollections.observableArrayList();
        for (String genreAsString : GenresAsStrings) {
            if (genreAsString.trim().equals("")){
                continue;
            }
            Genre g = dao.readByName(genreAsString);
            if (g == null) {
                g = new Genre();
                g.setGenre(genreAsString);
                dao.create(g);
            }
            genres.add(g);
        }

        return genres;
    }
}
