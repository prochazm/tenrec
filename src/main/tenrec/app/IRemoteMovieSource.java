package tenrec.app;

import tenrec.model.Movie;

import java.io.IOException;

/**
 * IRemoteMovieSource
 *
 * This interface makes class implementing it source
 * of single {@link Movie}. Depending on the implementation
 * the {@link Movie} can be taken from web, file system or
 * other dark places.
 */
public interface IRemoteMovieSource {
    /**
     * Return {@link Movie} from remote source.
     * @return Movie
     */
    Movie getMovie();
}
