package tenrec.app;

import tenrec.model.Genre;
import tenrec.model.Movie;

import java.util.List;

/**
 * IMovieFilter
 *
 * This is a write only interface used for setting
 * filtering conditions of actual filter.
 * Filtering result is List of Movies
 */
public interface IMovieFilter extends IFilter<Movie> {
    /**
     * Sets name filter
     * @param artifact name substring
     */
    void setNameFilter(String artifact);

    /**
     * Sets length filter upper boundary
     * @param length upper boundary of length
     */
    void setShorterThanFilter(Integer length);

    /**
     * Sets length filter lower boundary
     * @param length lower boundary of length
     */
    void setLongerThanFilter(Integer length);

    /**
     * Sets release filter upper boundary
     * @param year release year upper boundary
     */
    void setOlderThanFilter(Integer year);

    /**
     * Sets release filter lower boundary
     * @param year release year lower boundary
     */
    void setNewerThanFilter(Integer year);

    /**
     * Sets genre filter
     * @param genres List of genres
     */
    void setGenreFilter(List<Genre> genres);
}
