package tenrec.app;

import javafx.beans.binding.StringBinding;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.controlsfx.control.CheckComboBox;
import org.controlsfx.control.RangeSlider;
import tenrec.model.Genre;

/**
 * MenuAccordionController
 *
 * This class is controller for the left panel
 * with filters/edit options. It also imlpements
 * interface IFilterVisualiser.
 */
public class MenuAccordionController implements IFilterVisualizer{
    @FXML private TextField nameFilterTextField;
    @FXML private RangeSlider releaseFilterRangeSlider;
    @FXML private Label releaseFilterLabel;
    @FXML private RangeSlider lengthFilterRangeSlider;
    @FXML private Label lengthFilterLabel;
    @FXML private CheckComboBox<Genre> genreFilterComboCheck;

    private IModelVisualiser modelVisualiser;
    private IMovieFilter movieFilter;
    private ISpecialValueKeeper specialValueKeeper;

    /**
     * Class constructor takes reference to
     * IModelVisualizer implementation and
     * stores it.
     * @param modelVisualiser IModelVisualiser implementation
     */
    public MenuAccordionController(IModelVisualiser modelVisualiser){
        this.modelVisualiser = modelVisualiser;
    }

    /**
     * Initializes gui
     */
    @FXML
    public void initialize(){
        this.specialValueKeeper = modelVisualiser.getSpecialValueKeeper();
        this.movieFilter = modelVisualiser.getMovieFilter();
        this.nameFilterTextField.textProperty().addListener((s, o, n) -> this.movieFilter.setNameFilter(n));

        this.modelVisualiser.setFilterVisualizer(this);

        resetReleaseFilterRangeSlider(true);
        resetLengthFilterRangeSlider(true);
        resetGenreFilterComboCheck();

        ((ObservableList<Genre>)modelVisualiser.getGenres())
                .addListener((ListChangeListener.Change<? extends Genre> c) -> {
                    while(c.next()){
                        if (c.wasUpdated() || c.wasRemoved() || c.wasAdded()){
                            genreFilterComboCheck.getItems().setAll(modelVisualiser.getGenres());
                        }
                }
        });
    }

    /**
     * Deletes selected movies.
     * @param event event
     */
    @FXML
    public void deleteAction(ActionEvent event) {
        modelVisualiser.removeSelected();
    }

    /**
     * Lets user create new movie using dialog.
     * @param event event
     */
    @FXML
    public void addAction(ActionEvent event) {
        modelVisualiser.guiCreate();
    }

    /**
     * Lets user edit selected movie using dialog.
     * @param event event
     */
    @FXML
    public void editAction(ActionEvent event) {
        modelVisualiser.guiEditSelected();
    }

    /**
     * Resets all filters to their default values.
     */
    @FXML
    public void resetFilter() {
        this.nameFilterTextField.setText("");
        resetLengthFilterRangeSlider(false);
        resetReleaseFilterRangeSlider(false);
        genreFilterComboCheck.getCheckModel().clearChecks();
    }

    /**
     * Resets genre filters.
     */
    private void resetGenreFilterComboCheck(){
        this.genreFilterComboCheck.getItems().setAll(modelVisualiser.getGenres());
        this.genreFilterComboCheck.getCheckModel().getCheckedItems()
                .addListener((ListChangeListener.Change<? extends Genre> c) ->
                movieFilter.setGenreFilter(genreFilterComboCheck.getCheckModel().getCheckedItems())
        );
    }

    /**
     * Resets release filter slider. If true is passed
     * as argument then the first time initialization
     * is performed too.
     * @param firstTimeSetup perform first time initialization
     */
    private void resetReleaseFilterRangeSlider(boolean firstTimeSetup) {
        releaseFilterRangeSlider.maxProperty().bind(this.specialValueKeeper.getNewestMovieRelease());
        releaseFilterRangeSlider.minProperty().bind(this.specialValueKeeper.getOldestMovieRelease());
        releaseFilterRangeSlider.setHighValue(releaseFilterRangeSlider.getMax());
        releaseFilterRangeSlider.setLowValue(releaseFilterRangeSlider.getMin());

        if (!firstTimeSetup) {
            return;
        }

        releaseFilterRangeSlider.lowValueProperty().addListener((s, o, n) -> {
            if (o.intValue() != n.intValue()) {
                this.movieFilter.setNewerThanFilter(n.intValue());
            }
        });
        releaseFilterRangeSlider.highValueProperty().addListener((s, o, n) -> {
            if (o.intValue() != n.intValue()) {
                this.movieFilter.setOlderThanFilter(n.intValue());
            }
        });

        releaseFilterLabel.textProperty().bind(new StringBinding() {
            {
                super.bind(releaseFilterRangeSlider.lowValueProperty(),
                        releaseFilterRangeSlider.highValueProperty());
            }

            @Override
            protected String computeValue() {
                return Integer.toString(releaseFilterRangeSlider.lowValueProperty().intValue())
                        .concat(" až ")
                        .concat(Integer.toString(releaseFilterRangeSlider.highValueProperty().intValue()));
            }
        });
    }

    /**
     * Resets length filter slider. If true is passed
     * as argument then the first time initialization
     * is performed too.
     * @param firstTimeSetup perform first time initialization
     */
    private void resetLengthFilterRangeSlider(boolean firstTimeSetup){
        lengthFilterRangeSlider.maxProperty().bind(this.specialValueKeeper.getLongestMovieLength());
        lengthFilterRangeSlider.setHighValue(lengthFilterRangeSlider.getMax());
        lengthFilterRangeSlider.minProperty().bind(this.specialValueKeeper.getShortestMovieLength());
        lengthFilterRangeSlider.setLowValue(lengthFilterRangeSlider.getMin());

        if (!firstTimeSetup){
            return;
        }

        lengthFilterRangeSlider.lowValueProperty().addListener((s,o,n)->{
            if (o.intValue() != n.intValue()) {
                this.movieFilter.setLongerThanFilter(n.intValue());
            }
        });
        lengthFilterRangeSlider.highValueProperty().addListener((s,o,n)->{
            if (o.intValue() != n.intValue()) {
                this.movieFilter.setShorterThanFilter(n.intValue());
            }
        });
        lengthFilterLabel.textProperty().bind(new StringBinding() {
            {super.bind(lengthFilterRangeSlider.lowValueProperty(),
                    lengthFilterRangeSlider.highValueProperty());}
            @Override
            protected String computeValue() {
                return Integer.toString(lengthFilterRangeSlider.lowValueProperty().intValue())
                        .concat(" až ")
                        .concat(Integer.toString(lengthFilterRangeSlider.highValueProperty().intValue()))
                        .concat(" minut");
            }
        });
    }
}
