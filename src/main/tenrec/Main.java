package tenrec;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import tenrec.app.*;
import tenrec.database.*;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/mainWindow.fxml"));

        MovieTableController movieTableController = new MovieTableController(
                new MovieDao(Connection.getInstance()),
                new GenreDao(Connection.getInstance())
        );

        MenuAccordionController menuAccordionController = new MenuAccordionController(
                movieTableController
        );

        loader.setControllerFactory( c -> {
            if (c == MovieTableController.class){
                return movieTableController;
            } else if (c == MenuAccordionController.class){
                return menuAccordionController;
            }

            try {
                return c.newInstance();
            } catch (IllegalAccessException | InstantiationException e) {
                throw new RuntimeException(e);
            }
        });

        Parent root = loader.load();
        primaryStage.setTitle("Tenrec");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }

    @Override
    public void stop(){
        Connection.getInstance().close();
    }

    public static void main(String[] args) {
        //fillDB();
        launch(args);
    }

    private static void fillDB(){
        IMovieDao mm = new MovieDao(Connection.getInstance());
        IGenreDao gg = new GenreDao(Connection.getInstance());
        RandomDataBuilder rdb = new RandomDataBuilder(mm, gg);
        if (mm.read(1) == null){
            rdb.fill(10000);
        }
    }
}
